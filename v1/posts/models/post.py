from django.db import models
from django.utils.text import slugify

from v1.accounts.models.user import User
from v1.channels.models.channel import Channel
from v1.general.models import TimedModel
from v1.posts.models.attachment import Attachment
from v1.posts.models.tag import Tag


class Post(TimedModel):
    title = models.CharField(db_index=True, unique=True, max_length=120)
    slug = models.CharField(db_index=True, unique=True, max_length=150)
    description = models.TextField(max_length=500, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    attachment = models.ForeignKey(Attachment, on_delete=models.CASCADE)
    tags = models.ManyToManyField(Tag)
    channels = models.ManyToManyField(Channel)
    vote_count = models.PositiveIntegerField(default=0)

    class Meta:
        db_table = "posts"

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):

        self.slug = slugify(self.title)
        super(Post, self).save(*args, **kwargs)
