from django.db import models

from v1.general.models import TimedModel


class Tag(TimedModel):

    name = models.CharField(max_length=32, unique=True, db_index=True)
    description = models.TextField(max_length=100)


    class Meta:
        db_table ="tags"


    def __str__(self):
        return self.name
