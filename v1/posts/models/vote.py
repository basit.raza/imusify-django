from django.db import models

from v1.accounts.models.user import User
from v1.general.models import TimedModel
from v1.general.utils import vote_choices
from v1.posts.models.post import Post


class Vote(TimedModel):

    post = models.ForeignKey(Post, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.CharField(choices=vote_choices(), max_length=1)


    def __str__(self):
        return '{} - {}'.format(self.user.username, self.post.title)

    # def save(self, *args, **kwargs):
    #
    #
    #    if self.vote is 'U':
    #         self.post.vote_count = self.post.vote_count +1
    #
    #    if self.vote is 'D':
    #         self.post.vote_count = self.post.vote_count - 1
    #
    #    self.post.save()
    #    super(Vote, self).save(*args, **kwargs)