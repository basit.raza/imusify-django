import json
import os
from django.conf import settings
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver

from v1.posts.models.attachment import Attachment
from v1.posts.models.vote import Vote

#import waveform


@receiver(post_save, sender=Vote)
def update_post_vote_count(sender, instance, **kwargs):
    if instance.vote is 'U':
        instance.post.vote_count = instance.post.vote_count + 1
    if instance.vote is 'D':
        instance.post.vote_count = instance.post.vote_count - 1
    instance.post.save()

# @receiver(post_save, sender=Attachment)
# def generate_wave_form(sender, instance, **kwargs):
#     file = os.path.join(settings.MEDIA_ROOT, instance.file.name)
#     data = waveform.extractWaveform(file)
#     samples = {'count': data[0],'duration': data[1], 'points':[data[2]]}
#     waveform_data = json.dumps(samples)
#     print(waveform_data)
#     instance.data = waveform_data
