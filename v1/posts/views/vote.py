from rest_framework import generics, permissions, status
from rest_framework.response import Response

from v1.posts.models.post import Post
from v1.posts.models.vote import Vote
from v1.posts.serializers.vote import VoteSerializer


class CastVoteView(generics.CreateAPIView):

    model = Vote
    serializer_class = VoteSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Vote.objects.all()

    def post(self, request, post_id , action):

        votecode = 'D'
        votecode_opposite = 'U'

        if action == 'up':
            votecode = 'U'
            votecode_opposite = 'D'


        status_code = status.HTTP_201_CREATED

        try:
            vote = Vote.objects.get(post=post_id, user=request.user, vote=votecode)
            status_code = status.HTTP_406_NOT_ACCEPTABLE
        except Vote.DoesNotExist:
            post = Post.objects.get(pk=post_id)
            try:
                vote = Vote.objects.get(post=post_id, user=request.user, vote=votecode_opposite)
                vote.vote = votecode
                vote.save()
            except Vote.DoesNotExist:
                vote = Vote.objects.create(post=post, user=request.user, vote=votecode)

        serializer = self.get_serializer(vote)
        return Response(serializer.data, status=status_code)
