from rest_framework import generics, permissions, viewsets

from v1.posts.models.tag import Tag
from v1.posts.serializers.tag import TagSerializer


class TagDetail(viewsets.ModelViewSet):

    model = Tag
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]


    #def post(self, request):


