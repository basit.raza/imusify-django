from rest_framework import generics, permissions
from rest_framework.parsers import FormParser, MultiPartParser

from v1.posts.models.attachment import Attachment
from v1.posts.serializers.attachment import AttachmentSerializer


class AttachmentDetail(generics.RetrieveAPIView):
    model = Attachment
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    lookup_field = 'pk'

class AttachmentNew(generics.CreateAPIView):
    model = Attachment
    queryset = Attachment.objects.all()
    serializer_class = AttachmentSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    parser_classes = (FormParser, MultiPartParser)