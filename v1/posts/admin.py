from django.contrib import admin

# Register your models here.
from v1.posts.models.tag import Tag
from v1.posts.models.attachment import Attachment
from v1.posts.models.post import Post
from v1.posts.models.vote import Vote


class TagAdmin(admin.ModelAdmin):
    list_display = ('name','description')


class AttachmentAdmin(admin.ModelAdmin):
    list_display = ('id','file','content_type')

class VoteAdmin(admin.ModelAdmin):
    list_display = ('post','user','vote')


class PostAdmin(admin.ModelAdmin):
    list_display = ('title','owner','description','vote_count')


admin.site.register(Post, PostAdmin)
admin.site.register(Attachment, AttachmentAdmin)
admin.site.register(Vote, VoteAdmin)
admin.site.register(Tag, TagAdmin)
