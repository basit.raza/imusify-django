from rest_framework import serializers

from v1.posts.models.post import Post


class PostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = '__all__'