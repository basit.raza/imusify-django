from rest_framework import serializers

from v1.posts.models.tag import Tag


class TagSerializer(serializers.ModelSerializer):

    class Meta:
        model = Tag
        fields = ('name','description')
