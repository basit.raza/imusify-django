from rest_framework import serializers

from v1.posts.models.vote import Vote


class VoteSerializer(serializers.ModelSerializer):

    class Meta:
        model = Vote
        fields = ('user','post','vote')
        read_only_fields = ('user','post','vote')
