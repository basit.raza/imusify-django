from django.conf.urls import url
from django.urls import include
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

urlpatterns = [
    url(r'^auth-token/', obtain_jwt_token),
    url(r'^auth-token-verify/', verify_jwt_token),
    url(r'^users/', include('v1.accounts.urls')),
    url(r'^posts/', include('v1.posts.urls')),
    url(r'^channels/', include('v1.channels.urls'))
]