from datetime import datetime


def attachment_upload_to(instance, file):
    return 'attachments/{0}/{1}'.format(datetime.now().timestamp(), file)

def profile_upload_to(instance, file):
    return 'profile/{0}/{1}'.format(instance.user_id, file)

def gender_choices():
    return (
        ('F', "Female"),
        ('M', "Male"),
        ('U', "Unknwon")
    )

def vote_choices():
    return (
        ('D', "DWON"),
        ('U', "UP")
    )