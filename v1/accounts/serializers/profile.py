from rest_framework import serializers

from v1.accounts.models.profile import Profile
from v1.accounts.serializers.user import UserSerializer


class ProfileSerializer(serializers.ModelSerializer):
    user = UserSerializer(read_only=True)

    class Meta:
        model = Profile
        fields = '__all__'




