from rest_framework import serializers

from v1.accounts.models.user import User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=128, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ('id','username','first_name','last_name','email','password')