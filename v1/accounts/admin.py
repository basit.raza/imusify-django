from django.contrib import admin
from django.utils.html import format_html

from .models.profile import Profile
from .models.user import User


class UserAdmin(admin.ModelAdmin):
    list_display = ('email','username','first_name','last_name')
    exclude = ('groups', 'user_permissions')

class ProfileAdmin(admin.ModelAdmin):
    def image_tag(self, obj):
        return format_html('<img width="32" src="{}" />'.format(obj.image.url))

    image_tag.short_description = 'Image'

    list_display = ('image_tag', 'user','gender','date_of_birth')


admin.site.register(Profile,ProfileAdmin)
admin.site.register(User, UserAdmin)
