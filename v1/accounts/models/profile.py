from django.conf import settings
from django.db import models
from v1.general.models import TimedModel
from v1.general import utils

class Profile(TimedModel):

    gender_choices = utils.gender_choices()

    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    date_of_birth = models.DateField(blank=True)
    gender = models.CharField(max_length=1, choices=gender_choices, default="U")
    bio = models.TextField(blank=True)
    image = models.ImageField(blank=True, upload_to=utils.profile_upload_to)

    class Meta:
        db_table = "profile"

    def __str__(self):
        return self.user.username;