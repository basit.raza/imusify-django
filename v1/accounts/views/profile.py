from rest_framework import generics, permissions
from rest_framework.parsers import FileUploadParser, FormParser, MultiPartParser

from v1.accounts.models.profile import Profile
from v1.accounts.serializers.profile import ProfileSerializer


class ProfileDetail(generics.RetrieveUpdateAPIView):
    model = Profile
    queryset = Profile.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    lookup_field = "user__username"
    parser_classes = (FormParser, MultiPartParser)


    #def get_queryset(self):
    #    queryset = super(ProfileDetail, self).get_queryset()
    #    return queryset.filter(user__username=self.kwargs.get('username'))

    #def post(self, request, *args, **kwargs):
    #    """
    #       Create user profile
    #       ---
    #       parameters:
    #            - name: image
    #              schema:
    #                 type: file
    #        responseMessages:
    #            - code: 201
    #              message: Created
    #    """
    #    return self.sa(request, *args, **kwargs)



