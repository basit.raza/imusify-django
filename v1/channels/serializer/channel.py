from rest_framework import serializers

from v1.channels.models.channel import Channel


class ChannelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Channel
        fields = ('id','name','purpose','owner','members','private')
        read_only_fields = ('owner',)