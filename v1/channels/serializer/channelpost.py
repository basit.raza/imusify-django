from rest_framework import serializers

from v1.posts.models.post import Post


class ChannelPostSerializer(serializers.ModelSerializer):

    class Meta:
        model = Post
        fields = ('id','title','description','tags','attachment','owner')
        #read_only_fields = ('owner',)