from django.db import models

from v1.accounts.models.user import User
from v1.general.models import TimedModel


class Channel(TimedModel):

    name = models.CharField(unique=True, db_index=True, max_length=32)
    purpose = models.TextField(max_length=255)
    private = models.BooleanField(db_index=True, max_length=1, default=False)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    members = models.ManyToManyField(User, related_name="channel_members")


    class Meta:
        db_table = "channels"


    def __str__(self):
        return self.name