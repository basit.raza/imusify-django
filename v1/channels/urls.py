from django.conf.urls import url

from v1.channels.views import channel

urlpatterns = [
    url(r'^(?P<id>[0-9]+)/posts$', channel.ChannelPosts.as_view(), name="channelposts-list"),
    url(r'^(?P<id>[0-9]+)/posts/new$', channel.ChannelNewPost.as_view(), name="channelposts-create"),
    url(r'^(?P<id>[0-9]+)/detail$', channel.ChannelDetail.as_view(), name="channel-detail"),
    url(r'^$', channel.ChannelList.as_view(), name="channel-list")
]

